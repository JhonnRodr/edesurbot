var express = require('express');
var app = express();
app.set('port', (process.env.PORT || 3000));

var request = require('request');
var cheerio = require('cheerio');

var crontab = require('node-crontab');

var edesurDB = require('./model.js');
var route    = require('./route.js')(app);


var CronJob = require('cron').CronJob;
var Warlock = require('node-redis-warlock');
var redis;

const botgram = require("botgram");
const bot = botgram("392577686:AAGe-3276VHkD_doykqJcJklAdZYOq2eF3M");

if (process.env.REDISTOGO_URL){
	var rtg   = require("url").parse(process.env.REDISTOGO_URL);
	redis = require("redis").createClient(rtg.port, rtg.hostname);

	redis.auth(rtg.auth.split(":")[1]);

}else{

	redis = require('redis').createClient();
}

var warlock = new Warlock(redis);
var async = require('async');

function executeOnce (key, callback) {
    warlock.lock(key, 20000, function(err, unlock) {
        if (err) {
            // Something went wrong and we weren't able to set a lock
            console.log("error lock");
            return;
        }

        if (typeof unlock === 'function') {
            setTimeout(function() {
                callback(unlock);
            }, 1000);
        }
    });
}

function everyMinuteJobTasks (unlock) {
    async.parallel([
        updateSomething
        // etc...
    ],
    (err) => {
        if (err) {
            logger.error(err);
        }

        unlock();
    });
}

let updateSomething = function(done) {
    // Do stuff here
		console.log("Ejecutando Scrapper mediante Cronjob... #");
		existInteruptions();

    //return done();
    // Call done() when finished or call done(err) if error occurred
}

//===================
//INICIO DE LA APP
bot.command("start", function (msg, reply, next) {
  		reply.text("Te has suscrito satisfactoriamente");
  		let chatId = msg.chat.id;
  		suscribirse(chatId);
});

function suscribirse(chatId){
		edesurDB.insertTelegramId({
			id: chatId
		}); 
}

function runCronJob(){
	const minutoAMinuto = '* * * * *';
	const patronEdesur = '0 7,9,11,13 * * *';

  let sevenHourJob = new CronJob({
    cronTime: patronEdesur,
    onTick: function () {
        executeOnce('every-minute-lock', everyMinuteJobTasks);
    },
    start: true,
    runOnInit: true
  });
}

app.listen(app.get('port'), function() {
  console.log("EdesurBot run in port :" + app.get('port'));
  runCronJob();
})

function searchStringInArray (str, strArray) {
    for (var j=0; j<strArray.length; j++) {
        if (strArray[j].toUpperCase().match(str)) return j;
    }
    return -1;
}


function existInteruptions(){


	request('http://edesur2.edesur.com.do/mantenimientos/', function (error, response, body) {
		  const $ = cheerio.load(body);
		  $('.es-site-section-interrupciones-heading span').remove();
		  $('.es-site-section-interrupciones-heading br').remove();
		  var fecha;
		  fecha = $('.es-site-section-interrupciones-heading').text();
		  fecha = fecha.trim();
      console.log(fecha);

			var sector = 'PIANTINI';
		  var horarios = null;
		  var interrupciones = [];
		  var sectorDisponible = false;
		  var resumenInterrupciones = new Array();

			 $('.es-site-section-interrupciones-schedule ul li h3').filter(function(i, el) {
				   if($(this).text() === 'Santo Domingo'){
				   	horarios = $(this).parent('li').children('.small-block-grid-1');
						   	horarios.children('li').each(function(i, elem) {
								  var hora, sectores;
								  hora  = $(this).children('.es-site-section-interrupciones-time').text();
								   sectores  = $(this).children('p').text();
								   sectores = sectores.replace('\n','');
								   sectores = sectores.split(",");

								   for (var j=0; j<sectores.length; j++) {
        								sectores[j] = sectores[j].trim();
    								}

								   interrupciones  [i] = {
								          horarios: hora,
								          sectores: sectores
								        };
								});
		  			}
				}).text();

			 	console.log(interrupciones);

			 	var num = 0;
			 	interrupciones.forEach(function(elem, i){
			 		var res = searchStringInArray(sector, interrupciones[i]["sectores"]);


				 		if(res != -1 ){
				 			sectorDisponible = true;
				 			resumenInterrupciones[num] = {
				 				horarios: interrupciones[i]["horarios"],
				 				sectorDisponibilidad: sectorDisponible,
				 				sector: interrupciones[i]["sectores"][res]
				 			}
			 				sectorDisponible = false;
			 				num++;
			 		}

 				});

 				console.log(resumenInterrupciones);


				//Ejecuta si existe interrupcion
        if(resumenInterrupciones.length != 0)
        {

					edesurDB.lastRecord(function(error, lastData)
					{
							
							var fechaUltimoRegistro;
							var cantidadInterrupciones;

							if (typeof lastData[0] != "undefined") {
// ...								
								 fechaUltimoRegistro    = lastData[0].fecha;
								 cantidadInterrupciones = lastData[0].cantidad_interrupciones;
								console.log("fecha ultimo registro "+ fechaUltimoRegistro);
							}else{
								fechaUltimoRegistro    = "" ;
								cantidadInterrupciones = 0;
							}
							
							console.log("fecha actual "+fecha);

					//IMPRIMIR EN TELEGRAM
						edesurDB.getAllTelegramData(function(error, telegram)
						{
								console.log(telegram);
								telegram.forEach(function(elem, j){

									//Validacion de la base de datos comprobando los datos anteriores con los nuevos
									if(fechaUltimoRegistro == fecha)
						      {
						        console.log("las fecha actual de la plataforma edesur actual es igual que el anterior analisis");
										// bot.reply(telegram[j].telegram_id).text("las fecha actual de la plataforma edesur actual es igual que el anterior analisis");
						        //Si la cantidad de interrupciones anterior es igual a la cantidad de interrupciones actual
						        bot.reply(telegram[j].telegram_id).text("_______________________________");
						        if(cantidadInterrupciones == resumenInterrupciones.length)
						         {
						          console.log("No hay cambios en la lista de mantenimiento hasta el momento");
											bot.reply(telegram[j].telegram_id).text("No hay cambios en la lista de mantenimiento hasta el momento");

											//Imprimir datos
											resumenInterrupciones.forEach(function(e, i){

												 //Si el sector existe en #i horarios (sectorDisponbilidad puede ser true o false)
													if(resumenInterrupciones[i]["sectorDisponibilidad"]){
														console.log('Mostrando los datos...sin cambios!');
														bot.reply(telegram[j].telegram_id).text('Mostrando los datos...sin cambios!');
														console.log('Programada desde las ' + resumenInterrupciones[i]["horarios"]);
														bot.reply(telegram[j].telegram_id).text('Programada desde las ' + resumenInterrupciones[i]["horarios"]);
														console.log('En ' + resumenInterrupciones[i]["sector"]);
														bot.reply(telegram[j].telegram_id).text('En ' + resumenInterrupciones[i]["sector"]);
													}else{
														console.log('No existen interrupciones para Hoy');
														bot.reply(telegram[j].telegram_id).text('No existen interrupciones para Hoy');
													}
												});
						          return;
						         }
						      }

									//imprimir datos
									resumenInterrupciones.forEach(function(e, i){

										 //Si el sector existe en #i horarios (sectorDisponbilidad puede ser true o false)
											if(resumenInterrupciones[i]["sectorDisponibilidad"]){
												console.log('Existen interrupciones para Hoy!');
												bot.reply(telegram[j].telegram_id).text('Existen interrupciones para Hoy!');
												console.log('Programada desde las ' + resumenInterrupciones[i]["horarios"]);
												bot.reply(telegram[j].telegram_id).text('Programada desde las ' + resumenInterrupciones[i]["horarios"]);
												console.log('En ' + resumenInterrupciones[i]["sector"]);
												bot.reply(telegram[j].telegram_id).text('En ' + resumenInterrupciones[i]["sector"]);
											}else{
												console.log('No existen interrupciones para Hoy');
												bot.reply(telegram[j].telegram_id).text('No existen interrupciones para Hoy');
											}
								  	});

									});

									//Insert analisis
							edesurDB.insertData({
					          fecha: fecha,
					          sector: 'PIANTINI',
					          cantidad_interrupciones: resumenInterrupciones.length
					        });

							});

					});

        }else{
					edesurDB.getAllTelegramData(function(error, telegram)
					{
						telegram.forEach(function(elem, j){
								console.log('No existen interrupciones asignadas hasta el momento en el sector: '+sector);
								bot.reply(telegram[j].telegram_id).text('No existen interrupciones asignadas hasta el momento en el sector: '+sector);

						});
						
					});
				}




			// resumenInterrupciones.forEach(function(e, i){

				//IMPRIMIR EN TELEGRAM
				// edesurDB.getAllTelegramData(function(error, data)
				// {
				// 		console.log(data);
				// 		data.forEach(function(elem, j){
				// 			if(resumenInterrupciones[i]["sectorDisponibilidad"]){
				// 				console.log('Existen interrupciones para Hoy!');
				// 				bot.reply(data[j].telegram_id).text('Existen interrupciones para Hoy!');
				// 				console.log('Programada desde las ' + resumenInterrupciones[i]["horarios"]);
				// 				bot.reply(data[j].telegram_id).text('Programada desde las ' + resumenInterrupciones[i]["horarios"]);
				// 				console.log('En ' + resumenInterrupciones[i]["sector"]);
				// 				bot.reply(data[j].telegram_id).text('En ' + resumenInterrupciones[i]["sector"]);
				// 			}else{
				// 				console.log('No existen interrupciones para Hoy');
				// 				bot.reply(data[j].telegram_id).text('No existen interrupciones para Hoy');
				// 			}
				//
				// 		});
				//
				// });

				//Si existe records en la base de datos
				// console.log(edesurDB.lastRecord);
				// return;



 			// 	});
			//  console.log('Existen interrupciones para Hoy');
			//  console.log('No existen interrupciones para Hoy');
		  //  	 console.log(interrupciones);


		});
}
