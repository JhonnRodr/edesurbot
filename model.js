//creamos la base de datos tienda y el objeto SHOP donde iremos almacenando la info

var sqlite3 = require('sqlite3').verbose(),
db = new sqlite3.Database('database'),
EDESUR = {};

//elimina y crea la tabla clientes
EDESUR.createTable = function()
{
	db.run("DROP TABLE IF EXISTS edesur");
	db.run("DROP TABLE IF EXISTS telegram");
	db.run("CREATE TABLE IF NOT EXISTS edesur (id INTEGER PRIMARY KEY AUTOINCREMENT, fecha TEXT, sector TEXT, cantidad_interrupciones INTEGER, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)");
	db.run("CREATE TABLE IF NOT EXISTS telegram (telegram_id INTEGER PRIMARY KEY UNIQUE , timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)");
	console.log("La tabla usuarios ha sido correctamente creada");
}

EDESUR.insertTelegramId = function(telegram)
{
	var stmt = db.prepare("INSERT OR IGNORE INTO telegram VALUES (?,?)");
	stmt.run(telegram.id, this.getTodayDate());
	stmt.finalize();
}

EDESUR.getAllTelegramData = function(callback)
{
	db.all("SELECT * FROM telegram", function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});
}

EDESUR.deleteAllTelegramData = function(callback)
{
	db.all("DELETE FROM telegram", function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});
}


//--------------------

//inserta un nuevo usuario en la tabla clientes
EDESUR.insertData = function(edesur)
{
	var stmt = db.prepare("INSERT INTO edesur VALUES (?,?,?,?,?)");
	stmt.run(null,edesur.fecha,edesur.sector, edesur.cantidad_interrupciones, this.getTodayDate());
	stmt.finalize();
}



//obtenemos todos los clientes de la tabla clientes
//con db.all obtenemos un array de objetos, es decir todos
EDESUR.getAllData = function(callback)
{
	db.all("SELECT * FROM edesur", function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});
}

EDESUR.lastRecord = function(callback)
{
	db.all("SELECT * FROM edesur ORDER BY id DESC LIMIT 1;", function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});

}
//obtenemos un usuario por su id, en este caso hacemos uso de db.get
//ya que sólo queremos una fila
EDESUR.getData = function(userId,callback)
{
	stmt = db.prepare("SELECT * FROM edesur WHERE id = ?");
	//pasamos el id del cliente a la consulta
    stmt.bind(userId);
    stmt.get(function(error, row)
    {
    	if(error)
        {
            throw err;
        }
        else
        {
        	//retornamos la fila con los datos del usuario
            if(row)
            {
                callback("", row);
            }
            else
            {
            	console.log("El registro no existe");
            }
        }
    });
}

EDESUR.deleteAllData = function(callback)
{
	db.all("DELETE FROM edesur", function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});
}


EDESUR.getTodayDate = function()
{
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
      dd = '0'+dd
  }

  if(mm<10) {
      mm = '0'+mm
  }
  //today = mm + '/' + dd + '/' + yyyy;
  today = yyyy + '-'+ mm + '-' + dd;

  return today;
}

// //exportamos el modelo para poder utilizarlo con require
module.exports = EDESUR;
