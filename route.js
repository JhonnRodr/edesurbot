

var edesurDB = require('./model.js');

module.exports = function(app)
{
  // return function ruta()
  // {
    app.get('/createTable', function(request, response) {
      edesurDB.createTable();
      response.send('Tabla creada satisfactoriamente')
    });

//TELEGRAM TABLE
//========================
    app.get('/deleteAllTelegramData', function(request, response) {

        edesurDB.deleteAllTelegramData(function(error, data)
        {
          response.send("todos los registros han sido borrados");
        });
    });

    app.get('/getAllTelegramData', function(request, response) {

        edesurDB.getAllTelegramData(function(error, data)
        {
          console.log(data);
          response.send(data);
        });
    });

//EDESUR TABLE
//=================================
    app.get('/insertData', function(request, response) {
      let data = {fecha:"Sabado 23 de Julio 2017", sector: "PIANTINI"};
      edesurDB.insertData(data);
      response.send(data);

    });

    app.get('/getLastData', function(request, response) {

      edesurDB.lastRecord(function(error, data)
        {
          response.send(data);
        });
    });

    app.get('/fecha', function(request, response) {

      var edesurFechaAnterior = edesurDB.lastRecord(function(error, data)
        {
          response.send(data[0].fecha);
          console.log("fecha ultimo registro "+data);
        });
    });


    app.get('/deleteAllData', function(request, response) {

        edesurDB.deleteAllData(function(error, data)
        {
          response.send("todos los registros han sido borrados");
        });
    });
    app.get('/getAllData', function(request, response) {

      edesurDB.getAllData(function(error, data)
      {
          response.send(data);
      });
    });
  // }

}
